<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>THCCS Y10 - V-001ab - YSV-005ab</name>
  <macros>
    <PLCName>Tgt-HeC1010:Ctrl-PLC-001</PLCName>
  </macros>
  <width>2080</width>
  <height>1100</height>
  <background_color>
    <color name="WHITE" red="255" green="255" blue="255">
    </color>
  </background_color>
  <grid_color>
    <color name="TEXT-LIGHT" red="230" green="230" blue="230">
    </color>
  </grid_color>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <text>Y10 - V-001a/b and YSV-005a/b</text>
    <y>90</y>
    <width>1780</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color name="BLUE-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <background_color>
      <color name="BLUE-GROUP-BACKGROUND" red="179" green="209" blue="209">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>FlagLabel</name>
    <text>State Flag Status</text>
    <x>810</x>
    <y>134</y>
    <width>570</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BlkIconLabel</name>
    <text>State Block Icon</text>
    <x>1410</x>
    <y>136</y>
    <width>330</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter1</name>
    <macros>
      <Description>Preparation Purge Pressure</Description>
      <EngUnit>bara</EngUnit>
      <ParameterID>S1</ParameterID>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Paramter_Template.bob</file>
    <x>30</x>
    <y>173</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter2</name>
    <macros>
      <Description>Preparation Transition Pressure</Description>
      <EngUnit>bara</EngUnit>
      <ParameterID>S2</ParameterID>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Paramter_Template.bob</file>
    <x>30</x>
    <y>209</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1</name>
    <macros>
      <Description>Valves closed/opened correctly</Description>
      <FlagID>Flag1</FlagID>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Flag_Template.bob</file>
    <x>810</x>
    <y>173</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>ControlledDevicesLabel</name>
    <text>Controlled / Measured Devices</text>
    <x>30</x>
    <y>800</y>
    <width>1710</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_DevicesInManual</name>
    <text>Warning: some controlled devices are in Manual!</text>
    <x>30</x>
    <y>740</y>
    <width>501</width>
    <height>36</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="24.0">
      </font>
    </font>
    <foreground_color>
      <color name="RAL-2000" red="213" green="111" blue="1">
      </color>
    </foreground_color>
    <horizontal_alignment>1</horizontal_alignment>
    <auto_size>true</auto_size>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>${StepDeviceName}:DevInManual</pv_name>
      </rule>
    </rules>
    <border_width>3</border_width>
    <border_color>
      <color name="ORANGE" red="254" green="194" blue="81">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>ParameterLabel_1</name>
    <text>Parallel State Parameters</text>
    <x>30</x>
    <y>134</y>
    <width>759</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_1</name>
    <macros>
      <Description>Failure Action Active</Description>
      <FlagID>Flag2</FlagID>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Flag_Template.bob</file>
    <x>810</x>
    <y>208</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <file>../THCCS_Header.bob</file>
    <width>2080</width>
    <height>90</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>V-001a</name>
    <macros>
      <WIDDev>FIC</WIDDev>
      <WIDDev4>V</WIDDev4>
      <WIDDis>Proc</WIDDis>
      <WIDIndex>001</WIDIndex>
      <WIDIndex1>001a</WIDIndex1>
      <WIDSecSub>Tgt-HeC1010</WIDSecSub>
    </macros>
    <file>../Coustom/HeliumCirculators/circulator_V-001.bob</file>
    <x>30</x>
    <y>852</y>
    <width>200</width>
    <height>234</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>V-001a_1</name>
    <macros>
      <WIDDev>FIC</WIDDev>
      <WIDDev4>V</WIDDev4>
      <WIDDis>Proc</WIDDis>
      <WIDIndex>001</WIDIndex>
      <WIDIndex1>001b</WIDIndex1>
      <WIDSecSub>Tgt-HeC1010</WIDSecSub>
    </macros>
    <file>../Coustom/HeliumCirculators/circulator_V-001.bob</file>
    <x>570</x>
    <y>852</y>
    <width>200</width>
    <height>234</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>YSV-005a</name>
    <macros>
      <WIDDev>YSV</WIDDev>
      <WIDDis>Proc</WIDDis>
      <WIDIndex>005a</WIDIndex>
      <WIDSecSub>Tgt-HeC1010</WIDSecSub>
    </macros>
    <file>../../../99-Shared/Valves/valve solenoid/blockicons/PV_VALVE_AUMA_BlockIcon_Horizontal_Compact.bob</file>
    <x>230</x>
    <y>852</y>
    <width>100</width>
    <height>160</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>YSV-005b</name>
    <macros>
      <WIDDev>YSV</WIDDev>
      <WIDDis>Proc</WIDDis>
      <WIDIndex>005b</WIDIndex>
      <WIDSecSub>Tgt-HeC1010</WIDSecSub>
    </macros>
    <file>../../../99-Shared/Valves/valve solenoid/blockicons/PV_VALVE_AUMA_BlockIcon_Horizontal_Compact.bob</file>
    <x>770</x>
    <y>852</y>
    <width>100</width>
    <height>160</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter2_1</name>
    <macros>
      <Description>Preparation Transition Pressure</Description>
      <EngUnit>bara</EngUnit>
      <ParameterID>S2</ParameterID>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Paramter_Template.bob</file>
    <x>30</x>
    <y>243</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>FlagLabel_3</name>
    <text>Parallel Function Internal Alarms</text>
    <x>30</x>
    <y>440</y>
    <width>570</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_3</name>
    <macros>
      <AlarmID>Alarm1</AlarmID>
      <Description>Circulator failed to start / Step timeout</Description>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_AlarmMessage_Template.bob</file>
    <x>30</x>
    <y>480</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_4</name>
    <macros>
      <AlarmID>Alarm2</AlarmID>
      <Description>YSV-005a not opening</Description>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_AlarmMessage_Template.bob</file>
    <x>30</x>
    <y>515</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_5</name>
    <macros>
      <AlarmID>Alarm3</AlarmID>
      <Description>YSV-005b not opening</Description>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_AlarmMessage_Template.bob</file>
    <x>30</x>
    <y>550</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_6</name>
    <macros>
      <AlarmID>Alarm4</AlarmID>
      <Description>YSV-005a not closing</Description>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_AlarmMessage_Template.bob</file>
    <x>30</x>
    <y>585</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_7</name>
    <macros>
      <AlarmID>Alarm5</AlarmID>
      <Description>YSV-005b not closing</Description>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_AlarmMessage_Template.bob</file>
    <x>30</x>
    <y>620</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_8</name>
    <macros>
      <Description>YSV-005a Opened</Description>
      <FlagID>Flag3</FlagID>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Flag_Template.bob</file>
    <x>810</x>
    <y>243</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_9</name>
    <macros>
      <Description>YSV-005a Closed</Description>
      <FlagID>Flag4</FlagID>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Flag_Template.bob</file>
    <x>810</x>
    <y>278</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_10</name>
    <macros>
      <Description>YSV-005b Opened</Description>
      <FlagID>Flag5</FlagID>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Flag_Template.bob</file>
    <x>810</x>
    <y>313</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_11</name>
    <macros>
      <Description>YSV-005b Closed</Description>
      <FlagID>Flag6</FlagID>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Flag_Template.bob</file>
    <x>810</x>
    <y>348</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_12</name>
    <macros>
      <Description></Description>
      <FlagID>Flag7</FlagID>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Flag_Template.bob</file>
    <x>810</x>
    <y>383</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_13</name>
    <macros>
      <Description></Description>
      <FlagID>Flag8</FlagID>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Flag_Template.bob</file>
    <x>810</x>
    <y>418</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_14</name>
    <macros>
      <Description></Description>
      <FlagID>Flag9</FlagID>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Flag_Template.bob</file>
    <x>810</x>
    <y>453</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_15</name>
    <macros>
      <Description>Tell X-states that Y-10 is working as intended</Description>
      <FlagID>Flag10</FlagID>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Flag_Template.bob</file>
    <x>810</x>
    <y>488</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Y10</name>
    <macros>
      <Faceplate>../../../THCCS_OPI_MASTER/OPI/States/THCCS_ParallelFaceplate_Y010.bob</Faceplate>
      <StepName>Y10 - V-001a/b &amp; YSV-005a/b</StepName>
      <WIDDev>FSM</WIDDev>
      <WIDDis>SC</WIDDis>
      <WIDIndex>010</WIDIndex>
      <WIDSecSub>Tgt-HeC1010</WIDSecSub>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_BlockIcon_Parallel_Compact.bob</file>
    <x>1410</x>
    <y>178</y>
    <width>330</width>
    <height>130</height>
    <resize>1</resize>
  </widget>
</display>
